
package AnimalConditionSolution;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;

/**
 * FXML Controller class
 *
 * @author suraj
 */
public class AnimalConditionSolutionController implements Initializable {

    @FXML
    private ComboBox<String> Aname;
    @FXML
    private ComboBox<String> region;
    @FXML
    private ComboBox<String> pcondition;
    @FXML
    private Label infectionlist;
    @FXML
    private Button infectionresult;
    @FXML
    private Label SolutionofProblems;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Aname.getItems().addAll("Cow", "Goat");
        region.getItems().addAll("Hilly", "Terai");
        pcondition.getItems().addAll("Lessweight", "Wound infected","Less milk production");
        Aname.getSelectionModel().select("Cow");
        region.getSelectionModel().select("Hilly");
        pcondition.getSelectionModel().select("Lessweight");
        showInformation();
        // TODO
    }  
    public void showInformation(){
        if(Aname.getSelectionModel().getSelectedItem().equals("Cow")&&pcondition.getSelectionModel().getSelectedItem().equals("Lessweight")){
            infectionlist.setText("Your cow may be week because of given problems:"+"\n"+"\n"+"1.Dietary deficiencies are the most common cause of weakness and weight loss in cattle "+"\n"
            +"2. It may have eaten some plastics or waste material"+"\n"+"Enteric bacteria and parasites may be contributing factors.");
            SolutionofProblems.setText("Solution of the Problems:"+"\n"+"1. Provide Enough food required for cow"+"\n"
            +"2.  protein shake is a balanced grain ration "+"\n"+"3. Most grains have higher protein content than most grasses. so provide wheat,Chapat with grass as well");
            
        }
        
        
    }

    @FXML
    private void infectionbutton(ActionEvent event) {
       if(Aname.getSelectionModel().getSelectedItem().equals("Cow")&&pcondition.getSelectionModel().getSelectedItem().equals("Lessweight")){
            infectionlist.setText("Your cow may be week because of given problems:"+"\n"+"\n"+"1.Dietary deficiencies are the most common cause of weakness and weight loss in cattle "+"\n"
            +"2. It may have eaten some plastics or waste material"+"\n"+"Enteric bacteria and parasites may be contributing factors.");
            SolutionofProblems.setText("Solution of the Problems:"+"\n"+"1. Provide Enough food required for cow"+"\n"
            +"2.  protein shake is a balanced grain ration "+"\n"+"3. Most grains have higher protein content than most grasses. so provide wheat,Chapat with grass as well");
        } 
       if(Aname.getSelectionModel().getSelectedItem().equals("Cow")&&pcondition.getSelectionModel().getSelectedItem().equals("Wound infected")){
            infectionlist.setText("Your cow may be week because of given problems:"+"\n"+"\n"+"1. It may step upon any sharp materials"+"\n"
            +"2. It may be rubbed in smooth surface. "+"\n");
        } 
       if(Aname.getSelectionModel().getSelectedItem().equals("Cow")&&pcondition.getSelectionModel().getSelectedItem().equals("Less milk production")){
            infectionlist.setText("Your cow may be week because of given problems:"+"\n"+"\n"+"1. It is suffered from zoonotic diseases "+"\n"
            +"2. It may have eaten some plastics or waste material"+"\n");
        } 
       if(Aname.getSelectionModel().getSelectedItem().equals("Goat")&&pcondition.getSelectionModel().getSelectedItem().equals("Lessweight")){
            infectionlist.setText("Your Goat may be week because of given problems:"+"\n"+"\n"+"1. It is suffered from zoonotic diseases "+"\n"
            +"2. It may have eaten some plastics or waste material"+"\n");
        } 
       if(Aname.getSelectionModel().getSelectedItem().equals("Goat")&&pcondition.getSelectionModel().getSelectedItem().equals("Wound infected")){
            infectionlist.setText("Your Goat may be week because of given problems:"+"\n"+"\n"+"1. It is suffered from nostalgic diseases "+"\n"
            +"2. It may have eaten some plastics or waste material"+"\n");
        } 
       if(Aname.getSelectionModel().getSelectedItem().equals("Goat")&&pcondition.getSelectionModel().getSelectedItem().equals("Less milk production")){
            infectionlist.setText("Your Goat may be week because of given problems:"+"\n"+"\n"+"1. It is suffered from Malarial diseases "+"\n"
            +"2. It may have eaten some plastics or waste material"+"\n");
        } 
       
    }
    
}
