/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PostPanel;

import DBConnection.NewConnection;
import MainForm.HomeController;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Orientation;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * FXML Controller class
 *
 * @author suraj
 */
public class PostStoryViewController implements Initializable {
    public static String passmessage=null;
    public static File filepass=null;
Connection conn = null;
    PreparedStatement preparedStatement = null;
    PreparedStatement preparedStatementresult = null;
    PreparedStatement preparedStatementupdate = null;
    ResultSet resultSet = null;
    @FXML
    private FlowPane flowpane;
    @FXML
    private Button postOption;
    @FXML
    private ImageView imageview;
    private ImageView imview;
    String d,m,i;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    try {
        // TODO
        loaddata();
    } catch (SQLException ex) {
        Logger.getLogger(PostStoryViewController.class.getName()).log(Level.SEVERE, null, ex);
    }
    }    
    
   
    public void  HboxDataShow(String imagestring,String message){
        HBox hbox=new HBox();
    VBox vbox=new VBox();
        File file = new File(imagestring);
        Image image = new Image(file.toURI().toString());
        System.err.print(file.toString());
        imview.setImage(image);
        Label label=new Label();
        label.setText(message);
        hbox.getChildren().addAll(imview,label);
        Button b1=new Button();
        b1.setText("Comment");
        b1.setStyle("-fx-background-color:gray;-fx-background-radius:25px;");
        vbox.getChildren().addAll(hbox,b1);
        vbox.setMaxSize(750, 200);
        vbox.setMinSize(750, 200);
        flowpane.getChildren().addAll(vbox);
        
    }
    public void loaddata() throws SQLException{
        String sql ="select * from FramerPost";
        conn=NewConnection.getConnection();
        preparedStatement = conn.prepareStatement(sql);
        resultSet=preparedStatement.executeQuery();
        while(resultSet.next())
        {
          d=resultSet.getString("DATE");
        m =resultSet.getString("Image");
         i=resultSet.getString("MESSAGE");
         flowpane.getChildren().add(createPanel(m,i));
         flowpane.setOrientation(Orientation.VERTICAL);
        }
        
        
    }
    @FXML
    private void post(ActionEvent event) {
        loadWindow("/Framer/FramerUpload.fxml", "Upload", null);
       
    }
    private VBox createPanel(String imgstr,String messagestr)
    {
       VBox vbox=new VBox();
       ImageView imageView=new ImageView();
       imageView.setFitHeight(100);
       imageView.setFitWidth(100);
       Button button=new Button("Comment");
       Label label=new Label("Problems:"+"\n"+messagestr);
       vbox.getChildren().addAll(imageView,label,button);
       File file=new File(imgstr);
       Image img=new Image(file.toURI().toString());
       imageView.setImage(img);
       button.setOnAction(new EventHandler() {
           @Override
           public void handle(Event event) {
              passmessage=label.getText();
              filepass=file;
              loadWindow("/PostPanel/postManagement.fxml", "Chat Information", null);
           }
       });
       return vbox;
    }
    public void loadWindow(String loc,String title,String path)
    {
        
        try { 
            Parent parent = FXMLLoader.load(getClass().getResource(loc));
            Stage stage = new Stage(StageStyle.DECORATED);
            stage.setTitle(title);
            stage.setScene(new Scene(parent));  
             stage.initModality(Modality.APPLICATION_MODAL);
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(HomeController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
