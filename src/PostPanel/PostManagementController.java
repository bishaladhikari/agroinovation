/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PostPanel;

import DBConnection.NewConnection;
import static PostPanel.PostStoryViewController.filepass;
import static PostPanel.PostStoryViewController.passmessage;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

/**
 * FXML Controller class
 *
 * @author suraj
 */
public class PostManagementController implements Initializable {
    Connection conn = null;
    PreparedStatement preparedStatement = null;
    PreparedStatement preparedStatementresult = null;
    PreparedStatement preparedStatementupdate = null;
    ResultSet resultSet = null;

    @FXML
    private TextField postArea;
    private ScrollPane postScroll;
    @FXML
    private ImageView imageView;
    @FXML
    private TextArea MessageText;
    public static String fmsg=null;
    public static String amsg=null;
    @FXML
    private TextArea farmertext;
    @FXML
    private TextArea agrotext;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            // TODO
            loaddata1();
        } catch (SQLException ex) {
            Logger.getLogger(PostManagementController.class.getName()).log(Level.SEVERE, null, ex);
        }
        farmertext.setText(fmsg);
        agrotext.setText(amsg);
        Image img=new Image(filepass.toURI().toString());
        imageView.setImage(img);
        MessageText.setText(passmessage);
    }    

    @FXML
    private void commentPost(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER) {
            if(postArea.getText().isEmpty()){
                
            }
            else
            {
//                
//                FlowPane flowpane = new FlowPane();
//                
////               if(type==farmer){
////                 Label l1=new Label();
////                l1.setText(postArea.getText());
////                l1.setWrapText(true);
////                l1.setMaxWidth(200);
////                
////                l1.setTextFill(Color.web("#0076a3"));
////                l1.setFont(Font.font("Cambria", 16));
////                l1.setStyle("-fx-border-color:gray;-fx-border-radius:25px;-fx-padding:5 5 10 5;");
////                l1.setTranslateX(8);
////              
////                flowpane.setOrientation(Orientation.VERTICAL);
////                flowpane.getChildren().addAll(l1);  
////               }
////               if(type==agrovet){
////                 Label l1=new Label();
////                l1.setText(postArea.getText());
////                l1.setWrapText(true);
////                l1.setMaxWidth(200);
////                
////                l1.setTextFill(Color.web("#BB4F44"));
////                l1.setFont(Font.font("Cambria", 16));
////                l1.setStyle("-fx-border-color:gray;-fx-border-radius:25px;-fx-padding:5 5 10 5;");
////                l1.setTranslateX(170);
////              
////                flowpane.setOrientation(Orientation.VERTICAL);
////                flowpane.getChildren().addAll(l1);  
////               }
//                    
//                
//                Label l1=new Label();
//                l1.setText(amsg);
//                l1.setWrapText(true);
//                l1.setMaxWidth(200);
//                
//                l1.setTextFill(Color.web("#0076a3"));
//                l1.setFont(Font.font("Cambria", 16));
//                l1.setStyle("-fx-border-color:gray;-fx-border-radius:25px;-fx-padding:5 5 10 5;");
//                l1.setTranslateX(20);
//              
//                flowpane.setOrientation(Orientation.VERTICAL);
//                flowpane.getChildren().addAll(l1);
//                
//                postScroll.setContent(flowpane);
                
                
            }
        }
    }
    
    public void loaddata1() throws SQLException{
        String sql ="select * from Comment";
        conn=NewConnection.getConnection();
        preparedStatement = conn.prepareStatement(sql);
        resultSet=preparedStatement.executeQuery();
        while(resultSet.next())
        {
            amsg=resultSet.getString("MessageAgro")+"/n";
            fmsg=resultSet.getString("MessageFarmer")+"/n";
        }
        
        
    }
    
}
