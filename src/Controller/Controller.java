/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DBConnection.NewConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.time.LocalDateTime;
import java.util.Optional;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.stage.StageStyle;

/**
 *
 * @author Bishal
 */
public class Controller {
    Connection con=null;
    PreparedStatement preparedStatement=null;
    PreparedStatement preparedStatementresult = null;
    PreparedStatement preparedStatementupdate = null;
    public void InsertIntoLogin(String name,String username,String password,String address,String phone,String type) throws SQLException
    {
        con=NewConnection.getConnection();
        String sql = "Insert into Login(NAME,USERNAME,PASSWORD,ADDRESS,PHONE,"
                + "TYPE"
                + ")values("
                + "?,?,?,?,?,?)";

        try {

            preparedStatement = con.prepareStatement(sql);
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, username);
            preparedStatement.setString(3, password);
            preparedStatement.setString(4, address);
            preparedStatement.setString(5, phone);
            preparedStatement.setString(6, type);
            int result=preparedStatement.executeUpdate();
            if(result==1)
            {
              con.close();
              Dialog("Record Insert");
              
            }else
            {
                Dialog("Record not Insert");
                con.close();
            }
            
        } catch (SQLIntegrityConstraintViolationException ex) {
        }
    }
    void Dialog(String str) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Alert");
        alert.setHeaderText(str);
        alert.initStyle(StageStyle.UTILITY);
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            alert.close();
        }          
    }
    public boolean UpdateImage(int ai,String message,String str) throws SQLException
    {
        con=NewConnection.getConnection();
        LocalDateTime rightNow = LocalDateTime.now();
        String sql = "Insert into FramerPost(AID,DATE,MESSAGE,Image)values("
                + "?,?,?,?)";

        try {

            preparedStatement = con.prepareStatement(sql);
            preparedStatement.setInt(1, ai);
            preparedStatement.setString(2, rightNow.toString());
            preparedStatement.setString(3, message);
            preparedStatement.setString(4, str);
            int result=preparedStatement.executeUpdate();
            if(result==1)
            {
              con.close();
              return true;
              
            }else
            {
                con.close();
                return false;
            }
            
        } catch (SQLIntegrityConstraintViolationException ex) {
        }
        return false;
    }
    
}
