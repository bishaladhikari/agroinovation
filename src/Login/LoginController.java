/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Login;

import Controller.Login;
import static Controller.Login.passtype;
import MainForm.HomeController;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import java.awt.Event;
import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ToggleButton;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * FXML Controller class
 *
 * @author Bishal
 */
public class LoginController implements Initializable {

    @FXML
    private JFXTextField username;
    @FXML
    private JFXPasswordField password;
    @FXML
    private ToggleButton togg;
    @FXML
    private Button loginButton;
    @FXML
    private FontAwesomeIcon showpassword;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    private void userEnter(KeyEvent event) {
    }

    @FXML
    private void passwordEnter(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER) {
        Login log = new Login();
        if (log.checkLogin(username.getText(), password.getText())) {
            if (passtype.equals("Framer")) {
                loadWindow("/PostPanel/postStoryView.fxml", "Message", null);
                Stage stage = (Stage) password.getScene().getWindow();
                    stage.close();
            }
        } else {
            Dialog("Login Failded");
        }
        }
    }

    @FXML
    private void loadmainFormEnter(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER) {
            Login log = new Login();
            if (log.checkLogin(username.getText(), password.getText())) {
                if (passtype.equals("Framer")) {
                    loadWindow("/Framer/FramerUpload.fxml", "Update", null);
                    Stage stage = (Stage) password.getScene().getWindow();
                    stage.close();
                }
            } else {
                Dialog("Login Failded");
            }
        }
    }

    @FXML
    private void loadMainForm(ActionEvent event) {
        Login log = new Login();
        if (log.checkLogin(username.getText(), password.getText())) {
            if (passtype.equals("Framer")) {
                loadWindow("/Framer/FramerUpload.fxml", "Update", null);
            }
        } else {
            Dialog("Login Failded");
        }
    }

    void Dialog(String str) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Alert");
        alert.setHeaderText(str);
        alert.initStyle(StageStyle.UTILITY);
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            alert.close();
        }
    }

    public void loadWindow(String loc, String title, String path) {

        try {
            Parent parent = FXMLLoader.load(getClass().getResource(loc));
            Stage stage = new Stage(StageStyle.DECORATED);
            stage.setTitle(title);
            stage.setScene(new Scene(parent));
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(HomeController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void mouseReleased(MouseEvent event) {
    }

    @FXML
    private void mousePressed(MouseEvent event) {
    }

    @FXML
    private void loadSignUp(MouseEvent event) {
        loadWindow("/signUp/SignUp.fxml", "Sign UP", null);
    }

}
