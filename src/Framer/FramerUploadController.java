/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Framer;

import Controller.Controller;
import static Controller.Login.passai;
import static Controller.Login.passname;
import ImageProcessingDemo.ImageProcessingDemoController;
import com.jfoenix.controls.JFXButton;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javax.imageio.ImageIO;

/**
 * FXML Controller class
 *
 * @author Bishal
 */
public class FramerUploadController implements Initializable {

    @FXML
    private ImageView imageView;
    @FXML
    private JFXButton loadImage;
    @FXML
    private TextArea Message;
    String filename=null;
    @FXML
    private JFXButton viePost;
    @FXML
    private Label labelname;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        labelname.setText("WelCome, Mr."+passname);
        loadImage.setOnAction((ActionEvent event) -> {
                FileChooser fileChooser = new FileChooser();
                FileChooser.ExtensionFilter extFilterJPG = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.jpg");
                FileChooser.ExtensionFilter extFilterPNG = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.png");
                FileChooser.ExtensionFilter extFilterJPEG = new FileChooser.ExtensionFilter("JPEG files (*.jpeg)", "*.jpeg");
                fileChooser.getExtensionFilters().addAll(extFilterJPG, extFilterPNG, extFilterJPEG);
                File file = fileChooser.showOpenDialog(null);
                if (file != null) {
                    filename=file.getName().toString();
                }
                    Image image = new Image(file.toURI().toString());
                    imageView.setImage(image);

        });
    }    

    @FXML
    private void send(ActionEvent event) throws IOException, SQLException {
        Controller controller=new Controller();
        String str="Information/Upload/"+filename;
        File file=new File(str);
        if(controller.UpdateImage(passai, Message.getText(), str))
        {
            boolean check=ImageIO.write(SwingFXUtils.fromFXImage(imageView.getImage(),
                                null), "png", file);
             Dialog("Image Upload");
        }
        
        
    }

    @FXML
    private void cancel(ActionEvent event) {
        Stage stage = (Stage) Message.getScene().getWindow();
                    stage.close();
    }
    void Dialog(String str) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Alert");
        alert.setHeaderText(str);
        alert.initStyle(StageStyle.UTILITY);
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            alert.close();
        }
    }
    
}
