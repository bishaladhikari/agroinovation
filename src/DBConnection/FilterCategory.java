/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DBConnection;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Bishal
 */
public class FilterCategory {
     public static List<FilterCategory> data = new ArrayList<>();
    public int id;
    public String name;
    public String combine;

    public FilterCategory() {

    }

    public FilterCategory(ResultSet rs) {
        data.clear();
        try {
            while (rs.next()) {
                FilterCategory s = new FilterCategory();
                s.id=rs.getInt("CID");
                s.name = rs.getString("CNAME");
               // s.combine=rs.getString("DATE")+","+rs.getString("PCODE")+","+rs.getString("PNAME")+","+rs.getString("PFOR")+","+rs.getString("PSIZE")+","+rs.getString("PCOLOR");
                data.add(s);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static List<FilterCategory> findData() {
        FilterCategory name = null;
        try {
            ResultSet rs = DriverManager.getConnection(Connection.getConnection("information/disease.db")).prepareStatement("select * from category").executeQuery();
            name = new FilterCategory(rs);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return data;
    }
    
}
