/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DBConnection;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Bishal
 */
public class FilterName {
    public static List<FilterName> data = new ArrayList<>();
    public int id;
    public String name;
    public String combine;

    public FilterName() {

    }

    public FilterName(ResultSet rs) {
        data.clear();
        try {
            while (rs.next()) {
                FilterName s = new FilterName();
                s.id=rs.getInt("IID");
                s.name = rs.getString("INAME");
               // s.combine=rs.getString("DATE")+","+rs.getString("PCODE")+","+rs.getString("PNAME")+","+rs.getString("PFOR")+","+rs.getString("PSIZE")+","+rs.getString("PCOLOR");
                data.add(s);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static List<FilterName> findData() {
        FilterName name = null;
        try {
            ResultSet rs = DriverManager.getConnection(Connection.getConnection("information/disease.db")).prepareStatement("select * from items").executeQuery();
            name = new FilterName(rs);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return data;
    }
    
}
