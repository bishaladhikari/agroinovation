/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package signUp;

import Controller.Controller;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Bishal
 */
public class SignUpController implements Initializable {

    @FXML
    private TextField name;
    @FXML
    private TextField username;
    @FXML
    private TextField password;
    @FXML
    private TextField address;
    @FXML
    private TextField phone;
    @FXML
    private ComboBox<String> type;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        ObservableList<String> options = 
        FXCollections.observableArrayList(
            "Framer",
            "Agrovet"
        );
        type.setItems(options);
    }    

    @FXML
    private void save(ActionEvent event) throws SQLException {
        Controller controller=new Controller();
        controller.InsertIntoLogin(name.getText(), username.getText(), password.getText(), address.getText(), phone.getText(), type.getEditor().getText());
    }

    @FXML
    private void cancel(ActionEvent event) {
        Stage stage = (Stage) phone.getScene().getWindow();
         stage.close();
    }
    
}
