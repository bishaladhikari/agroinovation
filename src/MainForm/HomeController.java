/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MainForm;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.AnchorPane;
import DatabaseConnection.DbConnection;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableRow;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * FXML Controller class
 *
 * @author suraj
 */
public class HomeController implements Initializable {

    @FXML
    private ComboBox<String> categoryid;
    @FXML
    private TableView<homeModel> tableview;
    @FXML
    private AnchorPane description;
    @FXML
    private TextArea description_area;
    @FXML
    private TextArea types;
    @FXML
    private TitledPane seeding;
    @FXML
    private TitledPane finishing;
    @FXML
    private Label categorylabel;
    ObservableList<homeModel> homemodels = FXCollections.observableArrayList();
    PreparedStatement preparedStatementresult = null;
    Connection con = null;
    PreparedStatement preparedStatement = null;
    ResultSet resultSet = null;
    int i1 = 0;
    @FXML
    private TableColumn<homeModel, String> name;
    @FXML
    private TableColumn<homeModel, Integer> sn;
    @FXML
    private TextArea weatherarea;
    @FXML
    private TextArea seedingarea;
    @FXML
    private TextArea finishingarea;
    @FXML
    private TitledPane desc;
    @FXML
    private TitledPane weather_area;
    String nameforSearch;
    @FXML
    private VBox vboxAccordian;
    @FXML
    private ImageView imageview;
    @FXML
    private Button button;

    /**
     * Initializes the controller class.
     */

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        button.setOnAction((ActionEvent event) -> {
            loadWindow("/ImageProcessingDemo/ImageProcessingDemo.fxml", "Animal Information", null);
        });
        categoryid.getItems().removeAll(categoryid.getItems());
        categoryid.getItems().addAll("plantlist", "animallist");
        categoryid.getSelectionModel().select("plantlist");

       
        categoryid.getSelectionModel().selectedItemProperty().addListener((String) -> {
            try {

                
                categoryid.getSelectionModel().getSelectedItem();

                if ("plantlist".equals(categoryid.getSelectionModel().getSelectedItem())) {
                    
                    homemodels.clear();
                    try {
                        
                        tableShow();
                        
                    } catch (SQLException ex) {
                        Logger.getLogger(HomeController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                   // accordinAllDetail();
                } else if ("animallist".equals(categoryid.getSelectionModel().getSelectedItem())) {
                    
                    homemodels.clear();
                   
                        tableShowAnimalList();
                     
                  //  accordinAllDetail();
                }
            } catch (SQLException ex) {
                Logger.getLogger(HomeController.class.getName()).log(Level.SEVERE, null, ex);
            } 
        });

        try {
            if ("plantlist".equals(categoryid.getSelectionModel().getSelectedItem())) {
                homemodels.clear();
                tableShow();
            } else if ("animallist".equals(categoryid.getSelectionModel().getSelectedItem())) {
                homemodels.clear();
                tableShow();
            }
            // TODO
        } catch (SQLException ex) {
            Logger.getLogger(HomeController.class.getName()).log(Level.SEVERE, null, ex);
        }
            tableview.setRowFactory(tv -> {
            TableRow<homeModel> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                homeModel rowData = row.getItem();
                nameforSearch=rowData.getName();
                
                try {
                    accordinAllDetail();
                } catch (SQLException ex) {
                    Logger.getLogger(HomeController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(HomeController.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
            return row ;
        });
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    public void tableShow() throws SQLException {

        categorylabel.setText(categoryid.getSelectionModel().getSelectedItem());
        ResultSet resultset = null;
        i1 = 0;
        con = DbConnection.ConDB();
        homemodels.clear();
        String sql = "Select PNAME from " + categoryid.getSelectionModel().getSelectedItem();
        resultset = con.createStatement().executeQuery(sql);
        while (resultset.next()) {
            i1++;

            this.homemodels.add(new homeModel(i1, resultset.getString("PNAME")));
        }
        this.sn.setCellValueFactory(new PropertyValueFactory<>("i"));
        this.name.setCellValueFactory(new PropertyValueFactory<>("name"));
        this.tableview.setItems(this.homemodels);
        con.close();
    }

    public void tableShowAnimalList() throws SQLException {

        categorylabel.setText(categoryid.getSelectionModel().getSelectedItem());
        ResultSet resultset = null;
        i1 = 0;
        con = DbConnection.ConDB();
        homemodels.clear();
        String sql = "Select ANAME from " + categoryid.getSelectionModel().getSelectedItem();
        resultset = con.createStatement().executeQuery(sql);
        while (resultset.next()) {
            i1++;

            this.homemodels.add(new homeModel(i1, resultset.getString("ANAME")));
        }
        this.sn.setCellValueFactory(new PropertyValueFactory<>("i"));
        this.name.setCellValueFactory(new PropertyValueFactory<>("name"));
        this.tableview.setItems(this.homemodels);
        con.close();
    }

    public void accordinAllDetail() throws SQLException, FileNotFoundException {
        ResultSet resultset = null;
        con = DbConnection.ConDB();
        
        if (categoryid.getSelectionModel().getSelectedItem().equals("plantlist")) {
String sql = "Select * from plantlist where PNAME="+"'"+nameforSearch+"'";
// create a background fill 
           
  
            resultset = con.createStatement().executeQuery(sql);
            while (resultset.next()) {
                description_area.setText(resultset.getString("DEFINATION"));
                weather_area.setText("Environment");
                weatherarea.setText(resultset.getString("PWEATHER"));
                types.setText(resultset.getString("TYPE"));
                seedingarea.setText(resultset.getString("PSEEDING"));
                finishing.setText("Diseases and Solution");
                finishingarea.setText(resultset.getString("PPREFINISHING") + "\n" + "Diseases" + "\n" + resultset.getString("PDISEASES") + "\n" + "Solution" + "\n" + resultset.getString("PSOLUTION"));
//FileInputStream input = new FileInputStream("Cauliflower.jpg"); 
//  
//            // create a image 
//            Image image = new Image(input); 
//  
//            // create a background image 
//            BackgroundImage backgroundimage = new BackgroundImage(image,  
//                                             BackgroundRepeat.NO_REPEAT,  
//                                             BackgroundRepeat.NO_REPEAT,  
//                                             BackgroundPosition.DEFAULT,  
//                                                BackgroundSize.DEFAULT); 
//  
//            // create Background 
//            Background background = new Background(backgroundimage); 
//  
//            // set background 
//            vboxAccordian.setBackground(background); 
            }
            File file = new File("Information/image/"+nameforSearch+".jpg");
        Image image = new Image(file.toURI().toString());
        imageview.setImage(image);
        }
        if (categoryid.getSelectionModel().getSelectedItem().equals("animallist")) {
            String sql = "Select * from animallist where ANAME="+"'"+nameforSearch+"'";
            resultset = con.createStatement().executeQuery(sql);
            while (resultset.next()) {
                description_area.setText(resultset.getString("ADEFINATION"));
                weather_area.setText("Environment");
                weatherarea.setText(resultset.getString("AREGION"));

                types.setText(resultset.getString("ADISEASES"));
                seeding.setText("Detail");
                seedingarea.setText(resultset.getString("ADETAIL"));
                finishing.setText("Diseases and Solution");
                finishingarea.setText(resultset.getString("ADISEASES") + "\n" + "Solution" + resultset.getString("ASOLUTION"));
            }
            File file = new File("Information/image/"+nameforSearch+".jpg");
        Image image = new Image(file.toURI().toString());
        imageview.setImage(image);

        }
        con.close();

    }
    public void loadWindow(String loc,String title,String path)
    {
        
        try { 
            Parent parent = FXMLLoader.load(getClass().getResource(loc));
            Stage stage = new Stage(StageStyle.DECORATED);
            stage.setTitle(title);
            stage.setScene(new Scene(parent));  
             stage.initModality(Modality.APPLICATION_MODAL);
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(HomeController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void load(ActionEvent event) {
        loadWindow("/AnimalConditionSolution/AnimalConditionSolution.fxml", "Animal Information", null);
        description.getScene().getWindow().hide();
    }

}
