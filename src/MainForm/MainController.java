/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MainForm;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * FXML Controller class
 *
 * @author Bishal
 */
public class MainController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void loadHome(ActionEvent event) {
        loadWindow("/MainForm/home.fxml", "Animal Information", null);
    }

    @FXML
    private void loadCommunity(ActionEvent event) {
        loadWindow("/Login/LoginFramer.fxml","Login",null);
    }

    @FXML
    private void loadSoilTest(ActionEvent event) {
        Dialog("This feature is current not available");
        //loadWindow("/PostPanel/postStoryView.fxml", "Post Detail", null);
    }

    @FXML
    private void loadKheti(ActionEvent event) {
        loadWindow("/khetipatisection/khetipaitsection.fxml","Kheti",null);
    }

    @FXML
    private void loadPasu(ActionEvent event) {
        loadWindow("/AnimalConditionSolution/AnimalConditionSolution.fxml", "Animal Information", null);
    }
    public void loadWindow(String loc,String title,String path)
    {
        
        try { 
            Parent parent = FXMLLoader.load(getClass().getResource(loc));
            Stage stage = new Stage(StageStyle.DECORATED);
            stage.setTitle(title);
            stage.setScene(new Scene(parent));
            stage.setResizable(false);
             stage.initModality(Modality.APPLICATION_MODAL);
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(HomeController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    void Dialog(String str) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Alert");
        alert.setHeaderText(str);
        alert.initStyle(StageStyle.UTILITY);
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            alert.close();
        }          
    }
    
}
