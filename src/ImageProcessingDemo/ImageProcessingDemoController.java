/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ImageProcessingDemo;

import DBConnection.AutoCompleteComboBoxListener;
import DBConnection.DBConnection;
import DBConnection.FilterCategory;
import DBConnection.FilterName;
import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.IntStream;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelReader;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.StageStyle;
import javax.imageio.ImageIO;
import org.imgscalr.Scalr;

/**
 * FXML Controller class
 *
 * @author Bishal
 */
public class ImageProcessingDemoController implements Initializable {

    Connection con = null;
    PreparedStatement preparedStatement = null;
    PreparedStatement preparedStatementresult = null;
    PreparedStatement preparedStatementupdate = null;
    ResultSet resultSet = null;
    @FXML
    private ImageView imageView;
    @FXML
    private Button button;
    @FXML
    private ComboBox<String> selectitem;
    @FXML
    private ComboBox<String> category;
    public ObservableList<String> itemsname = FXCollections.observableArrayList();
    public ObservableList<String> itemscategory = FXCollections.observableArrayList();
    @FXML
    private Label labeld;
    @FXML
    private Label commond;
    @FXML
    private Label diesase;
    @FXML
    private Label management;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        itemsname.clear();
        List<FilterName> pname = FilterName.findData();
        pname.stream().forEach((f1) -> {
            itemsname.add(String.valueOf(f1.name));
        });
        selectitem.setItems(itemsname);
        new AutoCompleteComboBoxListener<>(selectitem);
        itemscategory.clear();
        List<FilterCategory> cname = FilterCategory.findData();
        cname.stream().forEach((f1) -> {
            itemscategory.add(String.valueOf(f1.name));
        });
        selectitem.setCellFactory(lv -> {
            ListCell<String> cell = new ListCell<String>() {
                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(empty ? null : item);
                }
            };
            cell.setOnMousePressed(e -> {
                if (! cell.isEmpty()) {
                    System.err.println("Click on123456789 "+cell.getItem());
                    search(cell.getItem());

                }
            });
            return cell ;
        });
        button.setOnAction((ActionEvent event) -> {
            if (!selectitem.getEditor().getText().isEmpty() && !category.getEditor().getText().isEmpty()) {
                FileChooser fileChooser = new FileChooser();
                String str=fileChooser+"/"+selectitem.getEditor().getText()+"/"+category.getEditor().getText();
                FileChooser.ExtensionFilter extFilterJPG = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.jpg");
                FileChooser.ExtensionFilter extFilterPNG = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.png");
                FileChooser.ExtensionFilter extFilterJPEG = new FileChooser.ExtensionFilter("JPEG files (*.jpeg)", "*.jpeg");
                fileChooser.getExtensionFilters().addAll(extFilterJPG, extFilterPNG, extFilterJPEG);
                File file = fileChooser.showOpenDialog(null);
                File filea = new File("C:\\OurProject\\CAN\\agroinovation\\Information\\Image\\plant\\SheathrotofRich.jpg");
                if (file != null) {
                    try {
                        checkPlantRichLeaf(file);
                        checkPlantRichStem(file);
                    checkPlantRichRoot(file);
                    checkPlantGrapesLeaf(file);
                    checkPlantGrapesSteam(file);
                    checkPlantGrapesRoot(file);
                    checkPlantCauliflowerLeaf(file);
                    checkPlantCauliflowerSteam(file);
                    checkPlantCauliflowerRoot(file);
                    checkPlantOrangeLeaf(file);
                    checkPlantOrangeSteam(file);
                    checkPlantOrangeRoot(file);
                    } catch (IOException ex) {
                        Logger.getLogger(ImageProcessingDemoController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    Image image = new Image(file.toURI().toString());
                    imageView.setImage(image);
                    System.err.println((compareImage(file, filea)));
                } else if (selectitem.getEditor().getText().isEmpty()) {
                    Dialog("Please Select the Items");                    
                }
            } else if (category.getEditor().getText().isEmpty()) {
                Dialog("Please Select the Category");
            } else {
                Dialog("Please Select the Items and Category");
            }
            
        });
        imageView.setOnMouseClicked((MouseEvent event) -> {
            Color color = imageView.getImage().getPixelReader().getColor((int) event.getX(), (int) event.getY());
            System.err.println(color.toString());
        });
    }
    private void search(String str)
    {
        if(str.equals("Rice"))
        {
            category.setItems(itemscategory);
        new AutoCompleteComboBoxListener<>(category);
        }
        else if(str.equals("Grapes"))
        {
            category.setItems(itemscategory);
        new AutoCompleteComboBoxListener<>(category);
        }else if(str.equals("Cauliflower")){
             category.setItems(itemscategory);
        new AutoCompleteComboBoxListener<>(category);
        }
        else
        {
            itemscategory.clear();
        }
    }

    public void Insert(String date) throws SQLException {
        con = DBConnection.ConDB();
        String sql = "Insert into SheathrotofRich(CODE)values("
                + "?)";
        try {
            preparedStatement = con.prepareStatement(sql);
            preparedStatement.setString(1, date);
            int result = preparedStatement.executeUpdate();
            if (result == 1) {
                con.close();
                
            } else {
                con.close();
            }
            
        } catch (SQLIntegrityConstraintViolationException ex) {
        }
    }
    @FXML
    private void itremFilter(KeyEvent event) {
        itemsname.clear();
        List<FilterName> pname = FilterName.findData();
        pname.stream().forEach((f1) -> {
            itemsname.add(String.valueOf(f1.name));
        });
        selectitem.setItems(itemsname);
        new AutoCompleteComboBoxListener<>(selectitem);
    }
    
    @FXML
    private void categotyFilter(KeyEvent event) {
        itemscategory.clear();
        List<FilterCategory> pname = FilterCategory.findData();
        pname.stream().forEach((f1) -> {
            itemscategory.add(String.valueOf(f1.name));
        });
        category.setItems(itemscategory);
        new AutoCompleteComboBoxListener<>(category);
    }

    void Dialog(String str) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Alert");
        alert.setHeaderText(str);
        alert.initStyle(StageStyle.UTILITY);

//alert.initModality(Modality.NONE);
//Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
//alert.setContentText("Are you ok with this?");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            alert.close();
        }          
    }
    String d=null;
    void checkPlantRichLeaf(File file) throws IOException
    {
        if(selectitem.getEditor().getText().equals("Rice") && category.getEditor().getText().equals("Leaf"))
        {
            double BacterialleafblightPer;
            double BrownSpotofRichPer;
            double BrownSpotofRich1Per;
            double RichBlastPer;
            File Bacterialleafblight = new File("Information/Image/plant/Rice/Leaf/Bacterialleafblight.jpg");
            File BrownSpotofRich = new File("Information/Image/plant/Rice/Leaf/BrownSpotofRich.jpg");
            File BrownSpotofRich1 = new File("Information/Image/plant/Rice/Leaf/BrownSpotofRich.jpg");
            File RichBlast = new File("Information/Image/plant/Rice/Leaf/RichBlast.jpg");
             BufferedImage img = ImageIO.read(file);
        BufferedImage img1 = ImageIO.read(Bacterialleafblight.getAbsoluteFile());
        BufferedImage img2 = ImageIO.read(BrownSpotofRich.getAbsoluteFile());
        BufferedImage img3 = ImageIO.read(BrownSpotofRich1.getAbsoluteFile());
        BufferedImage img4 = ImageIO.read(RichBlast.getAbsoluteFile());
        img1 = Scalr.resize(img1, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, img.getWidth(), img.getHeight());
        img2 = Scalr.resize(img2, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, img.getWidth(), img.getHeight());
        img3 = Scalr.resize(img3, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, img.getWidth(), img.getHeight());
        img4 = Scalr.resize(img4, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, img.getWidth(), img.getHeight());
            System.err.println(getDifferencePercent(img,img1));
            BacterialleafblightPer = getDifferencePercent(img,img1);
            BrownSpotofRichPer = getDifferencePercent(img,img2);
            BrownSpotofRich1Per = getDifferencePercent(img,img3);
            RichBlastPer = getDifferencePercent(img,img4);
            if (BacterialleafblightPer <=10) {
                d="Bacterial Leaf Blight";
                try {
                    getDisease(d,1);
                } catch (SQLException ex) {
                    Logger.getLogger(ImageProcessingDemoController.class.getName()).log(Level.SEVERE, null, ex);
                }
                labeld.setText("Bacterial leaf blight");
                System.err.println("Disease is:Bacterial leaf blight");
            }
            else if (BrownSpotofRichPer <= 10) {
                d="Brown Spot Of Rice";
                try {
                    getDisease(d,1);
                } catch (SQLException ex) {
                    Logger.getLogger(ImageProcessingDemoController.class.getName()).log(Level.SEVERE, null, ex);
                }
                labeld.setText("Brown Spot of Rich");
                System.err.println("Disease is:Brown Spot of Rice");
            }
            else if (BrownSpotofRich1Per <= 10) {
                d="Brown Spot Of Rice";
                try {
                    getDisease(d,1);
                } catch (SQLException ex) {
                    Logger.getLogger(ImageProcessingDemoController.class.getName()).log(Level.SEVERE, null, ex);
                }
                labeld.setText("Brown Spot of Rice");
                System.err.println("Disease is:Brown Spot of Rich");
            }
            else if (RichBlastPer <= 10) {
                d="Rice Blast";
                try {
                    getDisease(d,1);
                } catch (SQLException ex) {
                    Logger.getLogger(ImageProcessingDemoController.class.getName()).log(Level.SEVERE, null, ex);
                }
                labeld.setText("Rice Blast");
                System.err.println("Disease is:Rich Blast");
            }
            else if((BacterialleafblightPer >10&&BacterialleafblightPer<31)|(BrownSpotofRichPer > 10&&BrownSpotofRichPer<31)|(BrownSpotofRich1Per > 10&&BrownSpotofRich1Per<31)|(RichBlastPer > 10&&RichBlastPer<31))
            {
                Dialog("Image this not a Diseases");
            }else if((BacterialleafblightPer >31)|(BrownSpotofRichPer > 31)|(BrownSpotofRich1Per > 31)|(RichBlastPer > 31))
            {
                Dialog("Image this not a Leaf");
            }
        }
        
        
//        File[] directories = folder.listFiles(File::isDirectory);
//        for(File d:directories)
//        {
//            System.err.println("kjsdarh lfa ");
//        }
    }
    void checkPlantRichStem(File file) throws IOException
    {
        if(selectitem.getEditor().getText().equals("Rice") && category.getEditor().getText().equals("Steam"))
        {
            double SheathblightofRicePer;
            double SheathblightofRice1Per;
            double SheathrotofRicePer;
            double SheathrotofRice1per;
            File SheathblightofRice = new File("Information/Image/plant/Rice/Steam/Sheath blight of Rice.jpg");
            File SheathblightofRice1 = new File("Information/Image/plant/Rice/Steam/Sheath blight of Rice1.jpg");
            File SheathrotofRice = new File("Information/Image/plant/Rice/Steam/Sheath rot of Rice.jpg");
            File SheathrotofRice1 = new File("Information/Image/plant/Rice/Steam/Sheath rot of Rice1.jpg");
            BufferedImage img = ImageIO.read(file);
        BufferedImage img1 = ImageIO.read(SheathblightofRice.getAbsoluteFile());
        BufferedImage img2 = ImageIO.read(SheathblightofRice1.getAbsoluteFile());
        BufferedImage img3 = ImageIO.read(SheathrotofRice.getAbsoluteFile());
        BufferedImage img4 = ImageIO.read(SheathrotofRice1.getAbsoluteFile());
        img1 = Scalr.resize(img1, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, img.getWidth(), img.getHeight());
        img2 = Scalr.resize(img2, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, img.getWidth(), img.getHeight());
        img3 = Scalr.resize(img3, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, img.getWidth(), img.getHeight());
        img4 = Scalr.resize(img4, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, img.getWidth(), img.getHeight());
        SheathblightofRicePer = getDifferencePercent(img,img1);
            SheathblightofRice1Per = getDifferencePercent(img,img2);
            SheathrotofRicePer = getDifferencePercent(img,img3);
            SheathrotofRice1per = getDifferencePercent(img,img4);
            if (SheathblightofRicePer <= 10) {
                d="Sheath Blight Of Rice";
                try {
                    getDisease(d,2);
                } catch (SQLException ex) {
                    Logger.getLogger(ImageProcessingDemoController.class.getName()).log(Level.SEVERE, null, ex);
                }
                labeld.setText("Sheath blight of Rice");
                System.err.println("Disease is:Bacterial leaf blight");
            }
            else if (SheathblightofRice1Per <= 10) {
                d="Sheath Blight Of Rice";
                try {
                    getDisease(d,2);
                } catch (SQLException ex) {
                    Logger.getLogger(ImageProcessingDemoController.class.getName()).log(Level.SEVERE, null, ex);
                }
                labeld.setText("Sheath blight of Rice");
                System.err.println("Disease is:Brown Spot of Rich");
            }
            else if (SheathrotofRicePer <= 10) {
                d="Sheath rot of Rice";
                try {
                    getDisease(d,2);
                } catch (SQLException ex) {
                    Logger.getLogger(ImageProcessingDemoController.class.getName()).log(Level.SEVERE, null, ex);
                }
                labeld.setText("Sheath rot of Rice");
                System.err.println("Disease is:Brown Spot of Rich");
            }
            else if (SheathrotofRice1per <= 10) {
                d="Sheath rot of Rice";
                try {
                    getDisease(d,2);
                } catch (SQLException ex) {
                    Logger.getLogger(ImageProcessingDemoController.class.getName()).log(Level.SEVERE, null, ex);
                }
                labeld.setText("Sheath rot of Rice");
                System.err.println("Disease is:Rich Blast");
            }
            else if((SheathblightofRicePer >10&&SheathblightofRicePer<31)|(SheathblightofRice1Per > 10&&SheathblightofRice1Per<31)|(SheathrotofRicePer > 10&&SheathrotofRicePer<31)|(SheathrotofRice1per > 10&&SheathrotofRice1per<31))
            {
                Dialog("Image this not a Diseases");
            }else if((SheathblightofRicePer >31)|(SheathblightofRicePer > 31)|(SheathblightofRice1Per > 31)|(SheathrotofRice1per > 31))
            {
                Dialog("Image this not a Steam");
            }
        }
    }
    void checkPlantRichRoot(File file) throws IOException
    {
        if(selectitem.getEditor().getText().equals("Rice") && category.getEditor().getText().equals("Root"))
        {
            double SheathblightofRicePer;
            double SheathblightofRice1Per;
            double SheathrotofRicePer;
            double SheathrotofRice1per; 
            File SheathblightofRice = new File("Information/Image/plant/Rice/Root/Sheath blight of Rice.jpg");
            File SheathblightofRice1 = new File("Information/Image/plant/Rice/Root/Sheath blight of Rice1.jpg");
            File SheathrotofRice = new File("Information/Image/plant/Rice/Root/Sheath rot of Rice.jpg");
            File SheathrotofRice1 = new File("Information/Image/plant/Root/Steam/Sheath rot of Rice1.jpg");
            BufferedImage img = ImageIO.read(file);
        BufferedImage img1 = ImageIO.read(SheathblightofRice.getAbsoluteFile());
        BufferedImage img2 = ImageIO.read(SheathblightofRice1.getAbsoluteFile());
        BufferedImage img3 = ImageIO.read(SheathrotofRice.getAbsoluteFile());
        BufferedImage img4 = ImageIO.read(SheathrotofRice1.getAbsoluteFile());
        img1 = Scalr.resize(img1, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, img.getWidth(), img.getHeight());
        img2 = Scalr.resize(img2, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, img.getWidth(), img.getHeight());
        img3 = Scalr.resize(img3, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, img.getWidth(), img.getHeight());
        img4 = Scalr.resize(img4, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, img.getWidth(), img.getHeight());
        SheathblightofRicePer = getDifferencePercent(img,img1);
            SheathblightofRice1Per = getDifferencePercent(img,img2);
            SheathrotofRicePer = getDifferencePercent(img,img3);
            SheathrotofRice1per = getDifferencePercent(img,img4);
            if (SheathblightofRicePer <= 10) {
                d="Sheath Blight Of Rice";
                try {
                    getDisease(d,3);
                } catch (SQLException ex) {
                    Logger.getLogger(ImageProcessingDemoController.class.getName()).log(Level.SEVERE, null, ex);
                }
                labeld.setText("Sheath blight of Rice");
                System.err.println("Disease is:Bacterial leaf blight");
            }
            else if (SheathblightofRice1Per <= 10) {
                d="Sheath Blight Of Rice";
                try {
                    getDisease(d,3);
                } catch (SQLException ex) {
                    Logger.getLogger(ImageProcessingDemoController.class.getName()).log(Level.SEVERE, null, ex);
                }
                labeld.setText("Sheath blight of Rice");
                System.err.println("Disease is:Brown Spot of Rich");
            }
            else if (SheathrotofRicePer <= 10) {
                d="Sheath rot of Rice";
                try {
                    getDisease(d,3);
                } catch (SQLException ex) {
                    Logger.getLogger(ImageProcessingDemoController.class.getName()).log(Level.SEVERE, null, ex);
                }
                labeld.setText("Sheath rot of Rice");
                System.err.println("Disease is:Brown Spot of Rich");
            }
            else if (SheathrotofRice1per <= 10) {
                d="Sheath rot of Rice";
                try {
                    getDisease(d,3);
                } catch (SQLException ex) {
                    Logger.getLogger(ImageProcessingDemoController.class.getName()).log(Level.SEVERE, null, ex);
                }
                labeld.setText("Sheath rot of Rice");
                System.err.println("Disease is:Rich Blast");
            }
            else if((SheathblightofRicePer >10&&SheathblightofRicePer<31)|(SheathblightofRice1Per > 10&&SheathblightofRice1Per<31)|(SheathrotofRicePer > 10&&SheathrotofRicePer<31)|(SheathrotofRice1per > 10&&SheathrotofRice1per<31))
            {
                Dialog("Image this not a Diseases");
            }else if((SheathblightofRicePer >31)|(SheathblightofRicePer > 31)|(SheathblightofRice1Per > 31)|(SheathrotofRice1per > 31))
            {
                Dialog("Image this not a Steam");
            }
        }
    }
    void checkPlantGrapesLeaf(File file) throws IOException
    {
        if(selectitem.getEditor().getText().equals("Grapes") && category.getEditor().getText().equals("Leaf"))
        {
            double DownyMildewper;
            double Piercesper;
            File DownyMildew= new File("Information/Image/plant/Grapes/Leaf/Downy Mildew.jpeg");
            File Pierces = new File("Information/Image/plant/Grapes/Leaf/Pierces.jpg");
            BufferedImage img = ImageIO.read(file);
        BufferedImage img1 = ImageIO.read(DownyMildew.getAbsoluteFile());
        BufferedImage img2 = ImageIO.read(Pierces.getAbsoluteFile());
        img1 = Scalr.resize(img1, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, img.getWidth(), img.getHeight());
        img2 = Scalr.resize(img2, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, img.getWidth(), img.getHeight());
        DownyMildewper = getDifferencePercent(img,img1);
            Piercesper = getDifferencePercent(img,img2);
            if (DownyMildewper <= 10) {
                d="Downy Mildew";
                try {
                    getDisease(d,4);
                } catch (SQLException ex) {
                    Logger.getLogger(ImageProcessingDemoController.class.getName()).log(Level.SEVERE, null, ex);
                }
                labeld.setText("Downy Mildew");
                System.err.println("Disease is:Bacterial leaf blight");
            }
            else if (Piercesper <= 10) {
                d="Pierces";
                try {
                    getDisease(d,4);
                } catch (SQLException ex) {
                    Logger.getLogger(ImageProcessingDemoController.class.getName()).log(Level.SEVERE, null, ex);
                }
                labeld.setText("Pierces");
            }
            else if((DownyMildewper >10&&DownyMildewper<31)|(Piercesper > 10&&Piercesper<31))
            {
                Dialog("Image this not a Diseases");
            }else if((DownyMildewper >31)|(Piercesper > 31))
            {
                Dialog("Image this not a Leaf");
            }
        }
    }
    void checkPlantGrapesSteam(File file) throws IOException
    {
        if(selectitem.getEditor().getText().equals("Grapes") && category.getEditor().getText().equals("Steam"))
        {
            double Blackrotper;
            double GreyMoldper;
            File Blackrot= new File("Information/Image/plant/Grapes/Steam/Blackrot.jpeg");
            File GreyMold = new File("Information/Image/plant/Grapes/Steam/Grey Mold.jpg");
            BufferedImage img = ImageIO.read(file);
        BufferedImage img1 = ImageIO.read(Blackrot.getAbsoluteFile());
        BufferedImage img2 = ImageIO.read(GreyMold.getAbsoluteFile());
        img1 = Scalr.resize(img1, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, img.getWidth(), img.getHeight());
        img2 = Scalr.resize(img2, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, img.getWidth(), img.getHeight());
        Blackrotper = getDifferencePercent(img,img1);
            GreyMoldper = getDifferencePercent(img,img2);
            if (Blackrotper <= 10) {
                d="Blackrotper";
                try {
                    getDisease(d,4);
                } catch (SQLException ex) {
                    Logger.getLogger(ImageProcessingDemoController.class.getName()).log(Level.SEVERE, null, ex);
                }
                labeld.setText("Blackrotper");
                System.err.println("Disease is:Bacterial leaf blight");
            }
            else if (GreyMoldper <= 10) {
                d="Grey Moldper";
                try {
                    getDisease(d,4);
                } catch (SQLException ex) {
                    Logger.getLogger(ImageProcessingDemoController.class.getName()).log(Level.SEVERE, null, ex);
                }
                labeld.setText("Grey Moldper");
            }
            else if((Blackrotper >10&&Blackrotper<31)|(GreyMoldper > 10&&GreyMoldper<31))
            {
                Dialog("Image this not a Diseases");
            }else if((Blackrotper >31)|(GreyMoldper > 31))
            {
                Dialog("Image this not a Steam");
            }
        }
    }
    void checkPlantGrapesRoot(File file) throws IOException
    {
        if(selectitem.getEditor().getText().equals("Grapes") && category.getEditor().getText().equals("Root"))
        {
            double VineTrunkper;
            File VineTrunk= new File("Information/Image/plant/Grapes/Root/Vine Trunk.jpeg");
            BufferedImage img = ImageIO.read(file);
        BufferedImage img1 = ImageIO.read(VineTrunk.getAbsoluteFile());
        img1 = Scalr.resize(img1, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, img.getWidth(), img.getHeight());
        VineTrunkper = getDifferencePercent(img,img1);
            if (VineTrunkper <= 10) {
                d="Vine Trunk";
                try {
                    getDisease(d,4);
                } catch (SQLException ex) {
                    Logger.getLogger(ImageProcessingDemoController.class.getName()).log(Level.SEVERE, null, ex);
                }
                labeld.setText("Vine Trunk");
                System.err.println("Disease is:Bacterial leaf blight");
            }
            else if((VineTrunkper >10&&VineTrunkper<31))
            {
                Dialog("Image this not a Diseases");
            }else if((VineTrunkper >31))
            {
                Dialog("Image this not a Root");
            }
        }
    }
    void checkPlantCauliflowerLeaf(File file) throws IOException
    {
        if(selectitem.getEditor().getText().equals("Cauliflower") && category.getEditor().getText().equals("Leaf"))
        {
            double BlackRotper;
            double WhiteRustper;
            File BlackRot= new File("Information/Image/plant/Cauliflower/Leaf/Black Rot.jpeg");
            File WhiteRust = new File("Information/Image/plant/Cauliflower/Leaf/White Rust.jpg");
            BufferedImage img = ImageIO.read(file);
        BufferedImage img1 = ImageIO.read(BlackRot.getAbsoluteFile());
        BufferedImage img2 = ImageIO.read(WhiteRust.getAbsoluteFile());
        img1 = Scalr.resize(img1, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, img.getWidth(), img.getHeight());
        img2 = Scalr.resize(img2, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, img.getWidth(), img.getHeight());
        BlackRotper = getDifferencePercent(img,img1);
            WhiteRustper = getDifferencePercent(img,img2);
            if (BlackRotper <= 10) {
                d="Black Rot";
                try {
                    getDisease(d,5);
                } catch (SQLException ex) {
                    Logger.getLogger(ImageProcessingDemoController.class.getName()).log(Level.SEVERE, null, ex);
                }
                labeld.setText("Black Rot");
                System.err.println("Disease is:Bacterial leaf blight");
            }
            else if (WhiteRustper <= 10) {
                d="White Rust";
                try {
                    getDisease(d,5);
                } catch (SQLException ex) {
                    Logger.getLogger(ImageProcessingDemoController.class.getName()).log(Level.SEVERE, null, ex);
                }
                labeld.setText("White Rust");
            }
            else if((BlackRotper >10&&BlackRotper<31)|(WhiteRustper > 10&&WhiteRustper<31))
            {
                Dialog("Image this not a Diseases");
            }else if((BlackRotper >31)|(WhiteRustper > 31))
            {
                Dialog("Image this not a Leaf");
            }
        }
    }
    void checkPlantCauliflowerSteam(File file) throws IOException
    {
        if(selectitem.getEditor().getText().equals("Cauliflower") && category.getEditor().getText().equals("Steam"))
        {
            double DownyMildewper;
            double SclerotiniaStemrotper;
            File DownyMildew= new File("Information/Image/plant/Cauliflower/Steam/Downy Mildew.jpeg");
            File SclerotiniaStemrot = new File("Information/Image/plant/Cauliflower/Steam/Sclerotinia Stemrot.jpg");
            BufferedImage img = ImageIO.read(file);
        BufferedImage img1 = ImageIO.read(DownyMildew.getAbsoluteFile());
        BufferedImage img2 = ImageIO.read(SclerotiniaStemrot.getAbsoluteFile());
        img1 = Scalr.resize(img1, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, img.getWidth(), img.getHeight());
        img2 = Scalr.resize(img2, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, img.getWidth(), img.getHeight());
        DownyMildewper = getDifferencePercent(img,img1);
            SclerotiniaStemrotper = getDifferencePercent(img,img2);
            if (DownyMildewper <= 10) {
                d="Downy Mildew";
                try {
                    getDisease(d,5);
                } catch (SQLException ex) {
                    Logger.getLogger(ImageProcessingDemoController.class.getName()).log(Level.SEVERE, null, ex);
                }
                labeld.setText("Downy Mildew");
                System.err.println("Disease is:Bacterial leaf blight");
            }
            else if (SclerotiniaStemrotper <= 10) {
                d="Sclerotinia Stemrot";
                try {
                    getDisease(d,5);
                } catch (SQLException ex) {
                    Logger.getLogger(ImageProcessingDemoController.class.getName()).log(Level.SEVERE, null, ex);
                }
                labeld.setText("Sclerotinia Stemrot");
            }
            else if((DownyMildewper >10&&DownyMildewper<31)|(SclerotiniaStemrotper > 10&&SclerotiniaStemrotper<31))
            {
                Dialog("Image this not a Diseases");
            }else if((DownyMildewper >31)|(SclerotiniaStemrotper > 31))
            {
                Dialog("Image this not a Steam");
            }
        }
    }
    void checkPlantCauliflowerRoot(File file) throws IOException
    {
        if(selectitem.getEditor().getText().equals("Cauliflower") && category.getEditor().getText().equals("Root"))
        {
            double ClubrootPlasmodiper;
            File ClubrootPlasmodi= new File("Information/Image/plant/Cauliflower/Root/Clubroot Plasmodi.jpg");
            BufferedImage img = ImageIO.read(file);
        BufferedImage img1 = ImageIO.read(ClubrootPlasmodi.getAbsoluteFile());
        img1 = Scalr.resize(img1, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, img.getWidth(), img.getHeight());
        ClubrootPlasmodiper = getDifferencePercent(img,img1);
            if (ClubrootPlasmodiper <= 10) {
                d="Clubroot Plasmodi";
                try {
                    getDisease(d,5);
                } catch (SQLException ex) {
                    Logger.getLogger(ImageProcessingDemoController.class.getName()).log(Level.SEVERE, null, ex);
                }
                labeld.setText("Downy Mildew");
                System.err.println("Disease is:Bacterial leaf blight");
            }
            else if((ClubrootPlasmodiper >10&&ClubrootPlasmodiper<31))
            {
                Dialog("Image this not a Diseases");
            }else if((ClubrootPlasmodiper >31)|(ClubrootPlasmodiper > 31))
            {
                Dialog("Image this not a Leaf");
            }
        }
    }
    void checkPlantOrangeLeaf(File file) throws IOException
    {
        if(selectitem.getEditor().getText().equals("Orange") && category.getEditor().getText().equals("Leaf"))
        {
            double CitrusGreeningper;
            File CitrusGreening= new File("Information/Image/plant/Orange/Leaf/Citrus Greening.jpg");
            
            BufferedImage img = ImageIO.read(file);
        BufferedImage img1 = ImageIO.read(CitrusGreening.getAbsoluteFile());
        img1 = Scalr.resize(img1, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, img.getWidth(), img.getHeight());
        CitrusGreeningper = getDifferencePercent(img,img1);
            if (CitrusGreeningper <= 10) {
                d="Citrus Greening";
                try {
                    getDisease(d,6);
                } catch (SQLException ex) {
                    Logger.getLogger(ImageProcessingDemoController.class.getName()).log(Level.SEVERE, null, ex);
                }
                labeld.setText("Citrus Greening");
                System.err.println("Disease is:Bacterial leaf blight");
            }
            else if((CitrusGreeningper >10&&CitrusGreeningper<31))
            {
                Dialog("Image this not a Diseases");
            }else if((CitrusGreeningper >31)|(CitrusGreeningper > 31))
            {
                Dialog("Image this not a Leaf");
            }
        }
    }
    void checkPlantOrangeSteam(File file) throws IOException
    {
        if(selectitem.getEditor().getText().equals("Orange") && category.getEditor().getText().equals("Steam"))
        {
            double CitrusCankerper;
            File CitrusCanker= new File("Information/Image/plant/Orange/Steam/Citrus Canker.jpg");
            
            BufferedImage img = ImageIO.read(file);
        BufferedImage img1 = ImageIO.read(CitrusCanker.getAbsoluteFile());
        img1 = Scalr.resize(img1, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, img.getWidth(), img.getHeight());
        CitrusCankerper = getDifferencePercent(img,img1);
            if (CitrusCankerper <= 10) {
                d="Citrus Canker";
                try {
                    getDisease(d,6);
                } catch (SQLException ex) {
                    Logger.getLogger(ImageProcessingDemoController.class.getName()).log(Level.SEVERE, null, ex);
                }
                labeld.setText("Citrus Canker");
                System.err.println("Disease is:Bacterial leaf blight");
            }
            else if((CitrusCankerper >10&&CitrusCankerper<31))
            {
                Dialog("Image this not a Diseases");
            }else if((CitrusCankerper >31)|(CitrusCankerper > 31))
            {
                Dialog("Image this not a Steam");
            }
        }
    }
    void checkPlantOrangeRoot(File file) throws IOException
    {
        if(selectitem.getEditor().getText().equals("Orange") && category.getEditor().getText().equals("Root"))
        {
            double Phytophthoraper;
            double Phytophthoraper1;
            File Phytophthora= new File("Information/Image/plant/Orange/Root/Phytophthora.jpg");
            File Phytophthora1= new File("Information/Image/plant/Orange/Root/Phytophthora1.jpg");
            BufferedImage img = ImageIO.read(file);
        BufferedImage img1 = ImageIO.read(Phytophthora.getAbsoluteFile());
        BufferedImage img2 = ImageIO.read(Phytophthora1.getAbsoluteFile());
        img1 = Scalr.resize(img1, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, img.getWidth(), img.getHeight());
        img2 = Scalr.resize(img1, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, img.getWidth(), img.getHeight());
        Phytophthoraper = getDifferencePercent(img,img1);
        Phytophthoraper1 = getDifferencePercent(img,img1);
            if (Phytophthoraper <= 10) {
                d="Phytophthora";
                try {
                    getDisease(d,6);
                } catch (SQLException ex) {
                    Logger.getLogger(ImageProcessingDemoController.class.getName()).log(Level.SEVERE, null, ex);
                }
                labeld.setText("Phytophthora");
                System.err.println("Disease is:Bacterial leaf blight");
            }else if(Phytophthoraper1 <= 10){
                d="Phytophthora";
                try {
                    getDisease(d,6);
                } catch (SQLException ex) {
                    Logger.getLogger(ImageProcessingDemoController.class.getName()).log(Level.SEVERE, null, ex);
                }
                labeld.setText("Phytophthora");
            }
            else if((Phytophthoraper >10&&Phytophthoraper<31)|(Phytophthoraper1 >10&&Phytophthoraper1<31))
            {
                Dialog("Image this not a Diseases");
            }else if((Phytophthoraper >31)|(Phytophthoraper1 > 31))
            {
                Dialog("Image this not a Root");
            }
        }
    }
    public float compareImage(File fileA, File fileB) {
        float percentage = 0;
        try {
            // take buffer data from both image files //
            BufferedImage biA = ImageIO.read(fileA);
            DataBuffer dbA = biA.getData().getDataBuffer();
            int sizeA = dbA.getSize();
            BufferedImage biB = ImageIO.read(fileB);
            DataBuffer dbB = biB.getData().getDataBuffer();
            int sizeB = dbB.getSize();
            int count = 0;
            // compare data-buffer objects //
            if (sizeA ==sizeB) {
                
                for (int i = 0; i < sizeA; i++) {
                    
                    if (dbA.getElem(i) == dbB.getElem(i)) {
                        count = count + 1;
                    } 
                }
                percentage = (count * 100) / sizeA;
            } else {
                System.out.println("Both the images are not of same size");
            }
            
        } catch (Exception e) {
            System.out.println("Failed to compare image files ...");
        }
        return percentage;
    }
    private void getDisease(String str,int a) throws SQLException
    {
        con=DBConnection.ConDB();
            String sql = "Select DMANAGEMNT from disease where DNAME='"+str+"'";
             resultSet = con.createStatement().executeQuery(sql);
             while (resultSet.next()) {
                 management.setText(resultSet.getString("DMANAGEMNT")); 
                 getCommD(a);
             }
             con.close();
    }
    private void getCommD(int a) throws SQLException
    {
        con=DBConnection.ConDB();
        String str = null;
            String sql = "Select * from commond where CDID='"+a+"'";
             resultSet = con.createStatement().executeQuery(sql);
             while (resultSet.next()) {
                 commond.setText(resultSet.getString("CIDNAME")); 
             }
             con.close();
    }
    private static double getDifferencePercent(BufferedImage img1, BufferedImage img2) {
        int width = img1.getWidth();
        int height = img1.getHeight();
        int width2 = img2.getWidth();
        int height2 = img2.getHeight();
        if (width != width2 || height != height2) {
            throw new IllegalArgumentException(String.format("Images must have the same dimensions: (%d,%d) vs. (%d,%d)", width, height, width2, height2));
        }
 
        long diff = 0;
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                diff += pixelDiff(img1.getRGB(x, y), img2.getRGB(x, y));
            }
        }
        long maxDiff = 3L * 255 * width * height;
 
        return 100.0 * diff / maxDiff;
    }
 
    private static int pixelDiff(int rgb1, int rgb2) {
        int r1 = (rgb1 >> 16) & 0xff;
        int g1 = (rgb1 >>  8) & 0xff;
        int b1 =  rgb1        & 0xff;
        int r2 = (rgb2 >> 16) & 0xff;
        int g2 = (rgb2 >>  8) & 0xff;
        int b2 =  rgb2        & 0xff;
        return Math.abs(r1 - r2) + Math.abs(g1 - g2) + Math.abs(b1 - b2);
    }
}
